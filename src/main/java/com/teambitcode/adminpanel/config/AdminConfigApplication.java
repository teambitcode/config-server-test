package com.teambitcode.adminpanel.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class AdminConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminConfigApplication.class, args);
	}

}
